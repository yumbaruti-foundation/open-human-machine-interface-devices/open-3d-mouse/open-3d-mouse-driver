/*
 * Copyright (c) 2020 Yumbaruti Foundation
 *
 * TODO: Add specific license (Open Source and Open Hardware)
 */
#ifndef OPEN3DMOUSE_H
#define OPEN3DMOUSE_H
#include <generichiddevice.h>
class Open3DMouse : public GenericHIDDevice
{
public:
    Open3DMouse();
protected:
    qint16 findDevice();
};

#endif // OPEN3DMOUSE_H
