/*
 * Copyright (c) 2020 Yumbaruti Foundation
 *
 * TODO: Add specific license (Open Source and Open Hardware)
 */
#ifndef GENERICHIDDEVICE_H
#define GENERICHIDDEVICE_H

#include <QObject>
/* C */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
/* Qt Libraries */
#include <QObject>
#include <QString>
#include <QDebug>
#ifdef Q_OS_LINUX
/* Linux */
#include <linux/types.h>
#include <linux/input.h>
#include <linux/hidraw.h>
/*
 * Ugly hack to work around failing compilation on systems that don't
 * yet populate new version of hidraw.h to userspace.
 */
#ifndef HIDIOCSFEATURE
#warning Please have your distro update the userspace kernel headers
#define HIDIOCSFEATURE(len)    _IOC(_IOC_WRITE|_IOC_READ, 'H', 0x06, len)
#define HIDIOCGFEATURE(len)    _IOC(_IOC_WRITE|_IOC_READ, 'H', 0x07, len)
#endif
/* Unix */
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#endif
#ifdef Q_OS_WIN
//TBD
#endif
#ifdef Q_OS_MAC
//TBD
#endif

class GenericHIDDevice : public QObject
{
    Q_OBJECT
public:
    explicit GenericHIDDevice(QObject *parent = nullptr,
                              quint16 VendorId=0xFFFF,
                              quint16 ProductId=0xFFFF,
                              QString Path=QStringLiteral(""));
protected:
    quint16 vendorId;
    quint16 productId;
    QString path;
    int     devHID;
    hidraw_report_descriptor *reportDescriptor;
    virtual qint16 findDevice() = 0;
    qint16 openDevice();
    qint16 closeDevice();
    qint16 readDevice(QByteArray *data);
    qint16 writeDevice(QByteArray *data);
    qint16 getFeature(QByteArray *data);
    qint16 setFeature(QByteArray *data);
    qint16 getDescriptor(QByteArray *data);
private :
    QString bus_str(int bus);

signals:

};

#endif // GENERICHIDDEVICE_H
