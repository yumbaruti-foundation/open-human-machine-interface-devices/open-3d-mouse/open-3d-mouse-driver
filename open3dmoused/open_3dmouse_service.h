/*
 * Copyright (c) 2020 Yumbaruti Foundation
 *
 * TODO: Add specific license (Open Source and Open Hardware)
 */
#ifndef OPEN_3D_MOUSE_SERVICE_H
#define OPEN_3D_MOUSE_SERVICE_H

#include <QtService/Service>

class Open3DMouseService : public QtService::Service
{
	Q_OBJECT

public:
	explicit Open3DMouseService(int &argc, char **argv);

protected:
	CommandResult onStart() override;
	CommandResult onStop(int &exitCode) override;
};

#undef qService
#define qService static_cast<Open3DMouseService*>(QtService::Service::instance())

#endif // OPEN_3D_MOUSE_SERVICE_H
