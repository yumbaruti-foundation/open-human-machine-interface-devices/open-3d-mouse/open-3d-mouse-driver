/*
 * Copyright (c) 2020 Yumbaruti Foundation
 *
 * TODO: Add specific license (Open Source and Open Hardware)
 */
#ifndef OPEN_3DMOUSE_STR_H
#define OPEN_3DMOUSE_STR_H
#include <QtGlobal>
typedef union
{
    quint8 buttons_byte;
    struct
    {
        unsigned char bt_0  : 1;
        unsigned char bt_1  : 1;
        unsigned char bt_2  : 1;
        unsigned char bt_3  : 1;
        unsigned char pad_0 : 4;
    };
}buttons_t;

typedef union mouse
{
    quint8 buffer[5];
    struct
    {
        quint8 report_id;
        buttons_t m_pushButtons;
        qint8 x_delta;  // X Delta 8 bits
        qint8 y_delta;  // Y Delta 8 bits
        qint8 wheel;    // Wheel Delta 8 bits
    };
}mouse_t;

typedef union joystick
{
    quint8 buffer[5];
    struct{
        quint8 report_id;
        qint8 throttle;
        qint8 x_delta;
        qint8 y_delta;
        struct
        {
            unsigned char hs_1  : 1;
            unsigned char hs_2  : 1;
            unsigned char hs_3  : 1;
            unsigned char hs_4  : 1;//Padding
            unsigned char bt_1  : 1;
            unsigned char bt_2  : 1;
            unsigned char bt_3  : 1;
            unsigned char bt_4  : 1;
        };
    };
}joystick_t;

typedef union multi_axis
{
    quint8 buffer[13];
    struct{
        quint8 report_id;
        qint16 x_delta;
        qint16 y_delta;
        qint16 z_delta;
        qint16 x_rotation;     //[0,360]
        qint16 y_rotation;     //[0,360]
        qint16 z_rotation;     //[0,360]
    };
}multi_axis_t;

typedef union three_d_digitizer
{
    quint8 buffer[11];
    struct{
        quint8 report_id;
        qint16 x_tilt;     //[-90,90]
        qint16 y_tilt;     //[-90,90]
        qint16 altitude;   //[-90,90]
        qint16 azimuth;    //[0,360]
        qint16 twist;      //[0,360][0, 2%pi]
    };
}three_d_digitizer_t;

typedef union open_3dmouse_ctr
{
    quint8 buffer[5];
    struct{
        quint8 report_id;
        qint16 simulation;
        qint16 undefined;
    };
}open_3dmouse_ctr_t;
typedef struct report
{
    QString mainItem;
    quint16 reportSize;
    quint16 reportCount;
}report_t;
#endif // bOPEN_3DMOUSE_STR_H
