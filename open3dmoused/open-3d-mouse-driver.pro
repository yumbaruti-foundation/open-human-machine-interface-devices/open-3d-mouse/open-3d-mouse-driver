TEMPLATE = app

QT = core service
QT -= gui
CONFIG += c++14 console
CONFIG -= app_bundle

TARGET = open3dmoused
VERSION = 1.0.0

DEFINES += QT_DEPRECATED_WARNINGS QT_ASCII_CAST_WARNINGS QT_USE_QSTRINGBUILDER
#DEFINES += "TARGET=\\\\\\"$$TARGET\\\\\\""
#DEFINES += VERSION=\\\\\\"$$VERSION\\\\\\""
DEFINES += TARGET=$$(TARGET)
DEFINES += VERSION=$$(VERSION)

HEADERS += \
	generichiddevice.h \
	open3dmouse.h \
	open_3dmouse_service.h \
	open_3dmouse_str.h

SOURCES += \
    generichiddevice.cpp \
        main.cpp \
    open3dmouse.cpp \
	open_3dmouse_service.cpp

target.path = $$[QT_INSTALL_BINS]
INSTALLS += target

linux:!android {
	# install targets for systemd service files
        QMAKE_SUBSTITUTES += open3dmoused.service.in
        install_svcconf.files += $$shadowed(open3dmoused.service)
	install_svcconf.CONFIG += no_check_exist
	install_svcconf.path = $$[QT_INSTALL_LIBS]/systemd/system/
	INSTALLS += install_svcconf
}

win32 {
	# install targets for windows service files
        QMAKE_SUBSTITUTES += scinstall.bat.in
        install_svcconf.files += $$shadowed(scinstall.bat)
	install_svcconf.CONFIG += no_check_exist
	install_svcconf.path = $$[QT_INSTALL_BINS]
	INSTALLS += install_svcconf
}

macos {
	# install targets for launchd service files
        QMAKE_SUBSTITUTES += open3dmoused.plist.in
        install_svcconf.files += $$shadowed(open3dmoused.plist)
	install_svcconf.CONFIG += no_check_exist
	install_svcconf.path = /Library/LaunchDaemons
	INSTALLS += install_svcconf
}
OTHER_FILES += \\
	AndroidManifest-service.part.xml

DISTFILES += $$QMAKE_SUBSTITUTES
