/*
 * Copyright (c) 2020 Yumbaruti Foundation
 *
 * TODO: Add specific license (Open Source and Open Hardware)
 */
#include <open_3dmouse_service.h>

Open3DMouseService::Open3DMouseService(int &argc, char **argv) :
	Service{argc, argv}
{
	QCoreApplication::setApplicationName(QStringLiteral(TARGET));
	QCoreApplication::setApplicationVersion(QStringLiteral(VERSION));
	//...
}

QtService::Service::CommandResult Open3DMouseService::onStart()
{
	return CommandResult::Completed;
}

QtService::Service::CommandResult Open3DMouseService::onStop(int &exitCode)
{
	exitCode = EXIT_SUCCESS;
	return CommandResult::Completed;
}
