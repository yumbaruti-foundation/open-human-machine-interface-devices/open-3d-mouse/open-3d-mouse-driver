/*
 * Copyright (c) 2020 Yumbaruti Foundation
 *
 * TODO: Add specific license (Open Source and Open Hardware)
 */
#include "generichiddevice.h"

GenericHIDDevice::GenericHIDDevice(QObject *parent, quint16 VendorId, quint16 ProductId, QString Path)
    : QObject(parent)
    , vendorId(VendorId)
    , productId(ProductId)
    , path(Path)
{

}
/**
 * @brief GenericHIDDevice::openDevice
 * @return
 */
qint16 GenericHIDDevice::openDevice()
{
    qDebug() << "Open3DMouse::openDevice(void)";

#ifdef Q_OS_LINUX
    this->devHID = open(this->path.toLocal8Bit().data(),O_RDWR|O_NONBLOCK);
#endif
#ifdef Q_OS_MAC
//TBD
    this->devHID = -1;
#endif
#ifdef Q_OS_WIN
//TBD
    this->devHID = -1;
#endif
    if (this->devHID < 0)
    {
        qFatal("Unable to open device");
    }
    return this->devHID;
};
/**
 * @brief GenericHIDDevice::closeDevice
 * @return
 */
qint16 GenericHIDDevice::closeDevice()
{
#ifdef Q_OS_LINUX
    return (quint16) close(this->devHID);
#endif
#ifdef Q_OS_MAC
    return -1;
#endif
#ifdef Q_OS_WIN
    return -1;
#endif
};
/**
 * @brief GenericHIDDevice::readDevice
 * @param data
 * @return
 */
qint16 GenericHIDDevice::readDevice(QByteArray *data)
{
    qint16 res;
#ifdef Q_OS_LINUX
    res = (qint16)read(this->devHID, data->data(), data->length());
#endif
#ifdef Q_OS_MAC
    res = -1;
#endif
#ifdef Q_OS_WIN
    res = -1;
#endif
    if (res < 0) {
        qFatal("Error: %d", errno);
        qFatal("write");
    } else {
        qDebug() << "write() wrote :"<< res << " bytes";
    }
    return res;
};
/**
 * @brief GenericHIDDevice::writeDevice
 * @param data
 * @return
 */
qint16 GenericHIDDevice::writeDevice(QByteArray *data)
{
    qint16 res;
#ifdef Q_OS_LINUX
    res = (qint16)write(this->devHID, data->data(), data->length());
#endif
#ifdef Q_OS_MAC
    res = -1;
#endif
#ifdef Q_OS_WIN
    res = -1;
#endif
    if (res < 0) {
        qFatal("Error: %d", errno);
        qFatal("write");
    } else {
        qDebug() << "write() wrote :"<< res << " bytes";
    }
    return res;
};
/**
 * @brief GenericHIDDevice::getFeature
 * @param data
 * @return
 */
qint16 GenericHIDDevice::getFeature(QByteArray *data)
{
    qint16 res;
#ifdef Q_OS_LINUX
    bool ok;
    // TODO: Check if this convertion to Unsigned Short is the rigth one.
    // This method could be wrong.
    // Alternative, use a char buff[len] to store the data and then convert
    // that data to a QByteArray.
    res = (qint16)ioctl(this->devHID, HIDIOCGFEATURE(256), data->toUShort(&ok,10));
#endif
#ifdef Q_OS_MAC
    res = -1;
#endif
#ifdef Q_OS_WIN
    res = -1;
#endif
    if (res < 0)
    {
        qFatal("HIDIOCGFEATURE");
    } else {
        qDebug() << "ioctl HIDIOCGFEATURE returned: " << res;
        qDebug() << "Report data (not containing the report number:";
        for (quint8 i = 0; i < res; i++)
            qDebug() << "0x"<< data->at(i);
    }
    return res;
};
/**
 * @brief GenericHIDDevice::setFeature
 * @param data
 * @return
 */
qint16 GenericHIDDevice::setFeature(QByteArray *data)
{
    qint16 res;

#ifdef Q_OS_LINUX
    res=(qint16)ioctl(this->devHID, HIDIOCSFEATURE(data->length()), data->data());
#endif
#ifdef Q_OS_MAC
    res = -1;
#endif
#ifdef Q_OS_WIN
    res = -1;
#endif
    if (res < 0)
    {
        qFatal("HIDIOCSFEATURE");
    }else{
        qDebug() << "ioctl HIDIOCSFEATURE returned: " << res;
    }
    return res;
};
/**
 * @brief GenericHIDDevice::getDescriptor
 * @param data
 * @return
 */
qint16 GenericHIDDevice::getDescriptor(QByteArray *data)
{
    Q_UNUSED(data);
    qint16 res,desc_size=0;
#ifdef Q_OS_LINUX
    memset(this->reportDescriptor,0x0,sizeof(hidraw_report_descriptor));
    res = ioctl(this->devHID, HIDIOCGRDESCSIZE, &desc_size);
    if(res<0)
    {
        qFatal("HIDIOCGRDESCSIZE");
        return res;

    }else{
        qDebug() << "Report Descriptor Size: " << desc_size;
    }
    this->reportDescriptor->size = desc_size;
    res = ioctl(this->devHID, HIDIOCGRDESC, this->reportDescriptor);
    if (res < 0)
    {
        qFatal("HIDIOCGRDESC");
        return res;
    } else {
        qDebug() << "Report Descriptor";
        for (quint32 i = 0; i < this->reportDescriptor->size; i++)
        {
            qDebug() << "0x:"<< QString::number(this->reportDescriptor->value[i], 16);
        }
    }
    res = this->reportDescriptor->size;
#endif
#ifdef Q_OS_MAC
    res = -1;
#endif
#ifdef Q_OS_WIN
    res = -1;
#endif

    return res;
};
/**
 * @brief GenericHIDDevice::bus_str
 * @param bus
 * @return
 */
QString GenericHIDDevice::bus_str(int bus)
{
    switch(bus)
    {
        case BUS_USB:
            return QStringLiteral("USB");
            break;
        case BUS_HIL:
            return QStringLiteral("HIL");
            break;
        case BUS_BLUETOOTH:
            return QStringLiteral("Bluetooth");
            break;
        case BUS_VIRTUAL:
            return QStringLiteral("Virtual");
            break;
        default:
            return QStringLiteral("Other");
            break;
    }
};
