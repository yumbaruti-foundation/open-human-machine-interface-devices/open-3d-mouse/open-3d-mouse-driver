#include <open_3dmouse_service.h>

int main(int argc, char *argv[])
{
	Open3DMouseService svc{argc, argv};
	return svc.exec();
}
