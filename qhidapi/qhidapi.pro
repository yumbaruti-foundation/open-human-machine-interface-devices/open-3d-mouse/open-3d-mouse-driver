#-------------------------------------------------
#
# Project created by QtCreator 2019-06-28T08:02:40
#
#-------------------------------------------------

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qhidapi
TEMPLATE = lib

DEFINES += QHIDAPI_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        hexformatdelegate.cpp \
        qhidapi.cpp \
        qhidapi_p.cpp \
        qhiddeviceinfomodel.cpp \
        qhiddeviceinfoview.cpp \

HEADERS += \
        hexformatdelegate.h \
        hidapi.h \
        qhidapi.h \
        qhidapi_global.h  \
        qhidapi_p.h \
        qhiddeviceinfo.h \
        qhiddeviceinfomodel.h \
        qhiddeviceinfoview.h

unix|win32|macx:contains(DEFINES, USE_LIBUSB) | android {
    SOURCES += libusb/hid.c
    INCLUDEPATH += /usr/include/libusb-1.0
    LIBS += -lusb-1.0
} else {
    unix: SOURCES += linux/hid.c
    win32: SOURCES += linux/hid.c
    macx: SOURCES += linux/hid.c
}
